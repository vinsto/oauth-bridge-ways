<?php

use Vinsto\Agency\Agent\RemoteServiceAgent;
use Npaf\PersistentData\NpafSessionPersistentDataHandler;
use Symfony\Component\Yaml\Yaml;

require __DIR__.'/vendor/autoload.php';

session_start();

$agent = new RemoteServiceAgent();
list($infoCenterConfig, $infoCenterURL, $config)
    = $agent->fetchInfoCenterAndBaseConfig(__DIR__ . '/vendor/vinsto/vinsto-world/000_config/000_info_center.yaml');


if (!empty($_SESSION[NpafSessionPersistentDataHandler::SESSION_PREFIX . 'state'])) {
    unset($_SESSION[NpafSessionPersistentDataHandler::SESSION_PREFIX . 'state']);
}

$rs = new RemoteServiceAgent();
$oauth2CenterUrl = $rs->getOAuth2CenterUrl();

$fb = new \Npaf\Npaf([
    'app_id' => $config['global']['OAUTH2_CENTER_bbecd26__OAuth2_app_id'],
    'app_secret' => $config['global']['OAUTH2_CENTER_bbecd26__OAuth2_app_secret'],
    'default_woar_version' => 'v'. Npaf\Npaf::DEFAULT_WOAR_VERSION,
    'oauth2_url' => $oauth2CenterUrl,
    'woar_url' => $oauth2CenterUrl . '/woar',
]);

$helper = $fb->getRedirectLoginHelper();

$permissions = ['email']; // Optional permissions

$success__redirect_url = $_SERVER['REQUEST_SCHEME']. '://' . $_SERVER['HTTP_HOST'] . '/npaf-callback.php';
//$failure__redirect_url = $_SERVER['REQUEST_SCHEME']. '://' . $_SERVER['HTTP_HOST'] . '/login.php';
$loginUrl = $helper->getLoginUrl($success__redirect_url, $permissions);

echo '<a href="' . htmlspecialchars($loginUrl) . '">Log in with Npaf!</a>';
