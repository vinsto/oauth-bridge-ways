<?php

require __DIR__.'/vendor/autoload.php';

use Vinsto\Agency\Agent\RemoteServiceAgent;
use Symfony\Component\HttpFoundation\RequestStack;

$rs = new RemoteServiceAgent();
$oauth2CenterUrl = $rs->getOAuth2CenterUrl();

$agent = new RemoteServiceAgent();
list($infoCenterConfig, $infoCenterURL, $config)
    = $agent->fetchInfoCenterAndBaseConfig(__DIR__ . '/vendor/vinsto/vinsto-world/000_config/000_info_center.yaml');

$fb = new \Npaf\Npaf([
    'app_id' => $config['global']['OAUTH2_CENTER_bbecd26__OAuth2_app_id'],
    'app_secret' => $config['global']['OAUTH2_CENTER_bbecd26__OAuth2_app_secret'],
    'default_woar_version' => 'v'. Npaf\Npaf::DEFAULT_WOAR_VERSION,
    'oauth2_url' => $oauth2CenterUrl,
    'woar_url' => $oauth2CenterUrl . '/woar',
]);

try {
    // Returns a `Npaf\NpafResponse` object
    $response = $fb->get('/me?fields=id,name', '{access-token}');
    if ($response->getHttpStatusCode() !== 200) {
        throw new \Npaf\Exceptions\NpafResponseException($response);
    }
} catch(Npaf\Exceptions\NpafResponseException $e) {
    echo 'Woar returned an error: ' . $e->getMessage();
    exit;
} catch(Npaf\Exceptions\NpafSDKException $e) {
    echo 'Npaf SDK returned an error: ' . $e->getMessage();
    exit;
}

$user = $response->getWoarUser();

echo 'Name: ' . $user['name'];
// OR
// echo 'Name: ' . $user->getName();
