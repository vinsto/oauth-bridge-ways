#!/usr/bin/env bash

rm -rf vendor/vinsto
composer update vinsto/vinsto-world
composer update vinsto/vinsto-core
