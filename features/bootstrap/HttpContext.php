<?php

use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use Behat\MinkExtension\Context\MinkContext;
use phpseclib\Crypt\RSA;
use Vinsto\Crypt\RSA\RSAHandler;
use Guzzle\Http\Client;
use Behat\Testwork\ServiceContainer\Configuration\ConfigurationLoader;
use Behat\MinkExtension\Context\RawMinkContext;
use PHPUnit\Framework\Assert;

/**
 * Defines application features from the specific context.
 */
class HttpContext extends RawMinkContext implements Context
{
    /**
     * @Given /^I should not be at the uri "([^"]*)"$/
     */
    public function request($uri)
    {

        $current_url = $this->getSession()->getCurrentUrl();
        $current_uri = basename($current_url);

        Assert::assertNotEquals(trim($uri, '/'), $current_uri);
    }
}
