@functional-testing
Feature: OAuth2 Login
  As a security conscious developer I wish to ensure that only valid users can access some APIs.

  Scenario: Login in successfully to OAuth2 site
    When I am on "/login.php"
    And I follow "Log in with Npaf!"
    #
    #
    #
    # Todo: 用户名和密码用vinsto-world中的数据替换
    #
    And I fill in "Username" with "adminadmin@exampleexample.com"
    And I fill in "Password" with "adminadmin@exampleexample.com"
    And I press "Log in"
    And I should not be at the uri "/login"
